require_relative '../lib/factorial'
require_relative '../lib/nostringexception'

begin
  puts "Enter a number"
  input = gets.chomp
  if input =~ /^[-+]?[0-9]+$/
    puts input.to_i.factorial
  else
    raise NoStringException,"Strings are not allowed. please enter a number"
    puts e.class, e.message
  end
end
