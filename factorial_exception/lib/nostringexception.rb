class NoStringException < StandardError

  attr_reader :message
  def initialize(message = nil)
    @message = message || "Argument should not be a string"
  end

end
