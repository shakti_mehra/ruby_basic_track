class NegativeNumberException < StandardError

  attr_reader :message
  def initialize(message = nil)
    @message = message || "Number should not be negative"
  end

end
