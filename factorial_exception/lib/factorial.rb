require_relative '../lib/negativenumberexception'

class Integer

  def factorial
      raise NegativeNumberException, 'Number cannot be negative' if self < 0    
    (1..self).inject(:*)
  end

end
