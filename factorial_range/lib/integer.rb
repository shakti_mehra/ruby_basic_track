class Integer

  def calculate_factorial
    (1..self).inject(:*)
  end

end
