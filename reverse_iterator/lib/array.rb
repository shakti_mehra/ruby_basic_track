class Array

  def reverse_iterate
    i = size
    while i >= 0 do
      yield( self[i] )
      i -= 1
    end
  end

end
