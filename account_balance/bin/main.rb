require_relative '../lib/customer'

obj1 = Customer.new("ozil")
obj1.deposit(1500)
obj1.withdrawal(200)
puts "account balance of '#{ obj1.name }' with account number '#{ obj1.account_no }' is INR '#{ obj1.balance }'"

obj2 = Customer.new("sanchez")
obj2.deposit(500)
obj2.withdrawal(1200)
puts "account balance of '#{ obj2.name }' with account number '#{ obj2.account_no }' is INR '#{ obj2.balance }'"

obj3 = Customer.new("ramsey")
obj3.deposit(700)
obj3.withdrawal(600)
puts "account balance of '#{ obj3.name }' with account number '#{ obj3.account_no }' is INR '#{ obj3.balance }'"
