class Customer

  attr_reader :balance, :name, :account_no
  @@account_no = 0

  def initialize(name)
    @name = name
    @balance = 1000
    @@account_no += 1
    @account_no = @@account_no
  end

  def deposit(sum)
    @balance = @balance + sum
  end

  def withdrawal(amount)
    @balance = @balance - amount
  end

end
