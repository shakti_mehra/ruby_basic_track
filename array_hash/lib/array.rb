class Array

  def classify_by_length
    hash = Hash.new { |hash, key| hash[key] = [] }
    for item in self
      hash[item.to_s.length].push item
    end
    hash
  end

end
