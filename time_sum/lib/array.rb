require 'date'

class Array

  def sum_time
    f = 0

    for element in self
      dt = DateTime.parse(element)
      s = dt.hour * 3600 + dt.min * 60 + dt.sec
      f = f + s
    end

    mm, ss = f.divmod(60)
    hh, mm = mm.divmod(60)
    dd, hh = hh.divmod(24)
    "#{dd} days, #{hh} hours, #{mm} minutes and #{ss} seconds"
    end

end
