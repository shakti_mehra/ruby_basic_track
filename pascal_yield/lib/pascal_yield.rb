class PascalYield

  def pascal(n)
    yield array = [1]
    (n).times do
      array.unshift(0).push(0)
      yield array = array.each_cons(2).map { |a, b| a + b }
    end
  end

end
