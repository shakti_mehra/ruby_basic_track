require_relative '../lib/pascal_yield'

obj1 = PascalYield.new
obj1.pascal(6){ |line| puts line.join(" ") }
