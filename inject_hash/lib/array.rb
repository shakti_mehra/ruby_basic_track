class Array

  def group_odd_even
    hash = Hash.new(0)
    grouping_hash = Hash.new{ |hash, key| hash[key] = [] }
    hash = group_by { |item| item.to_s.length }
    hash.inject({}) do |a, (key,item)| 
      if (key % 2 == 0 ) then grouping_hash["even"].push item
      else
        grouping_hash["odd"].push item
      end
    end
    grouping_hash
  end

end
