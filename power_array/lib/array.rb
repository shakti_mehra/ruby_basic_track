class Array

  def power(exponent)
    map { |number| (number ** exponent) }
  end

end
