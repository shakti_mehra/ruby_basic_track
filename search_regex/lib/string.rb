class String

  def count_and_match(pattern)
    i = 0
    return gsub(/#{pattern}/i)  do |m|
      i += 1
      "(#{ m })"
    end, i
  end

end
