class String

  def str_reverse
    split.reverse.join(' ')
  end

end
