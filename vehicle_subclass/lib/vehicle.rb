class Vehicle

  attr_writer :price

  def initialize(name, price)
    @name = name
    @price = price
  end

  def to_s
    "name of the bike is #@name and its price is #@price"
  end 

end
