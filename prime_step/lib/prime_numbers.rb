require 'prime'

class PrimeNumbers

  def self.upto(n)
    if n >=2 then puts 2
    end
    (3..n).step(2) do |p|
      if Prime.prime?(p) then puts p
      end
    end
  end
end
