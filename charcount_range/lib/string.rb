class String

  LOWERCASE = ('a'..'z')
  UPPERCASE = ('A'..'Z')
  DIGIT = ('0'..'9')

  def char_count
    frequency_hash = Hash.new(0)
    self.each_char do |char|
      case char
        when LOWERCASE
          frequency_hash['lower_case'] += 1
        when UPPERCASE
          frequency_hash['upper_case'] += 1
        when DIGIT
          frequency_hash['digit_count'] += 1
        else
          frequency_hash['special_char'] += 1
        end
    end
    frequency_hash
  end

end
