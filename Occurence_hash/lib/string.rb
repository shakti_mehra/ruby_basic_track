class String

  def count_characters

    frequency_hash = Hash.new(0)
    each_char do |char|
      char = char.downcase
      frequency_hash[char] += 1 if ('a'..'z').include?(char)
    end
     
    frequency_hash
  end
end
