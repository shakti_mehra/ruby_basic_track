class NotNullException < StandardError

  attr_reader :message
  def initialize(message = nil)
    @message = message || "name cannot be blank"
  end

end
