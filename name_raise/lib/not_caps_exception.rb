class NotCapsException < StandardError

  attr_reader :message
  def initialize(message = nil)
    @message = message || "Name should start with a capital letter"
  end

end
