require_relative '../lib/not_null_exception'
require_relative '../lib/not_caps_exception'

class Name

  attr_reader :first, :last
  
  def first=(first)
    if first == nil
      raise NotNullException.new("first name should not be blank")
    end
    caps_check(first)
  end

  def caps_check(first)   
    if first[0] =~ /^[a-z]+$/
      raise NotCapsException.new("first letter should be capital")
    else
      @first = first
    end
  end
  private :caps_check, :first, :last
  def last=(last)
    if last == nil
      raise NotNullException.new("Lastname"), "Last name cannot be blank"
    end
    @last = last
  end

  def full_name
    "firstname: #{ @first } lastname: #{ @last }"
  end

  def initialize(first, last)
    self.first = first
    self.last = last
  end

end
