class String

  VOWELS = /[aeiou]/i

  def vowels_replace
    gsub(VOWELS, "*")
  end

end
