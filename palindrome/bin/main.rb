require_relative '../lib/string'

puts "enter something to check if it's a palindrome"
str = gets.chomp

if (str == "q") or (str == "Q")
  puts "Quitting"

else
  puts str.palindrome?
end
