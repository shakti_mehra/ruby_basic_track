require_relative '../lib/array'
require 'csv'

t = CSV.table('../lib/info.csv')
t = t.group_by {|e| e[:_designation]}
t = t.sort
t.write_to_file
