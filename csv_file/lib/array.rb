class Array

  def write_to_file
    open('myfile.out', 'w') do |f|
      each do |key, value|
        if value.size > 1
          f.puts "#{ key }s"
        value.each{ |e| f.puts "#{ e[:name] }" + " (EmpId: #{ e[:_empid] } )" }
        else
          f.puts "#{ key }"
          value.each{ |e| f.puts "#{ e[:name] }" + " (EmpId: #{ e[:_empid] } )" }
        end
      end
    end
  end

end
