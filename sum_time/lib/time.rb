class Time

  TIME_FORMAT = /(^[0]?\d|1\d|2[0-3]):([0-5]\d):([0-5]\d)$/
  def sum(time1, time2)
    flag = 0
    if (time1 !~ TIME_FORMAT) or (time2 !~ TIME_FORMAT)
      puts "wrong format for time "

    else

      sum_time = Array.new(0)
      time_1 = time1.split(":").map(&:to_i)
      time_2 = time2.split(":").map(&:to_i)

      sum_time[0] = time_1[0] + time_2[0]
      sum_time[1] = time_1[1] + time_2[1]
      sum_time[2] = time_1[2] + time_2[2]

      if sum_time[2] > 59
        sum_time[2] = sum_time[2] - 60
        sum_time[1] = sum_time[1] + 1
      end

      if sum_time[1] > 59
        sum_time[1] = sum_time[1] - 60
        sum_time[0] = sum_time[0] + 1
      end

      if sum_time[0] > 24
        sum_time[0] = sum_time[0] - 24
        flag = 1
      end

      if flag == 0
        "#{ sum_time[0] }:#{ sum_time[1] }:#{ sum_time[2] }"

      else
        "1 day & #{ sum_time[0] }:#{ sum_time[1] }:#{ sum_time[2] }"
      end
    end
  end

end
